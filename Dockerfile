FROM ubuntu:18.04

MAINTAINER CeShine Lee <ceshine@ceshine.net>

ENV LANG C.UTF-8
ARG PYTHON_VERSION=3.7
ARG CONDA_PYTHON_VERSION=3
ARG CONDA_DIR=/opt/conda

# Instal basic utilities
RUN apt-get update && \
  apt-get install -y --no-install-recommends wget build-essential && \
  apt-get clean && \
  rm -rf /var/lib/apt/lists/*

# Install miniconda
ENV PATH $CONDA_DIR/bin:$PATH
RUN apt-get update && apt-get install -y --no-install-recommends ca-certificates && \
  wget --quiet https://repo.continuum.io/miniconda/Miniconda$CONDA_PYTHON_VERSION-latest-Linux-x86_64.sh -O /tmp/miniconda.sh && \
  echo 'export PATH=$CONDA_DIR/bin:$PATH' > /etc/profile.d/conda.sh && \
  /bin/bash /tmp/miniconda.sh -b -p $CONDA_DIR && \
  rm -rf /tmp/* && \
  apt-get clean && \
  rm -rf /var/lib/apt/lists/*

# Install Python
RUN conda install -y python=$PYTHON_VERSION && \
  conda install -y h5py scikit-learn matplotlib \
  pandas mkl-service cython && \
  conda clean -tipsy

RUN  pip install --upgrade pip && pip install -U flask waitress Flask-Caching

COPY flaskr /web/flaskr
COPY instance/config.py /web/instance/config.py

WORKDIR /web
EXPOSE 8080
CMD waitress-serve --call 'flaskr:create_app'
