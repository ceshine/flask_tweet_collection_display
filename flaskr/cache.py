from flask import current_app
from flask_caching import Cache

def init_app(app):
    global CACHE
    CACHE = Cache(app, config={'CACHE_TYPE': 'simple'})

CACHE = None
