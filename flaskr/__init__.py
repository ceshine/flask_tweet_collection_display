from pathlib import Path

from flask import Flask

def create_app(test_config=None):
    # create and configure the app
    app = Flask(__name__, instance_relative_config=True)
    app.config.from_mapping(
        SECRET_KEY='dev',
        DATABASE=Path(app.instance_path) / 'tweets.db',
    )
    from . import db
    db.init_app(app)

    from . import cache
    cache.init_app(app)

    if test_config is None:
        # load the instance config, if it exists, when not testing
        app.config.from_pyfile('config.py', silent=True)
    else:
        # load the test config if passed in
        app.config.from_mapping(test_config)

    # ensure the instance folder exists
    Path(app.instance_path).mkdir(exist_ok=True)

    from . import tweets
    app.register_blueprint(tweets.bp)
    app.add_url_rule('/', endpoint='index')

    from . import stats
    app.register_blueprint(stats.bp)

    # a simple page that says hello
    @app.route('/hello')
    def hello():
        return 'Hello, World!'

    return app
