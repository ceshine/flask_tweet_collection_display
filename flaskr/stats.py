from datetime import datetime, timedelta
from collections import Counter
from flask import (
    Blueprint, flash, g, redirect, render_template, request, url_for, jsonify,
)
from werkzeug.exceptions import abort

from flaskr.db import get_db
from flaskr.cache import CACHE

bp = Blueprint('stats', __name__)

TIMEZONE_OFFSET = timedelta(hours=-8)


def total_tweets(db):
    return db.execute("""SELECT COUNT(*) FROM tweets""").fetchone()[0]


def total_tags(db):
    return db.execute("""SELECT COUNT(DISTINCT(tag_id)) FROM tweet_tag_assoc""").fetchone()[0]


def total_groups(db):
    return db.execute("""SELECT COUNT(DISTINCT(group_id)) FROM tweet_group_assoc""").fetchone()[0]


@bp.route('/stats/')
@CACHE.cached(timeout=7200)
def basic_stats():
    db = get_db()
    dates = [datetime.utcfromtimestamp(x[0]) + TIMEZONE_OFFSET for x in db.execute(
        "SELECT timestamp FROM tweets;").fetchall()]
    # By Date
    days = [x.date() for x in dates if x > datetime(2018, 6, 1)]
    data = sorted(Counter(days).items(), key=lambda x: x[0])
    by_date_x = ", ".join(["'%s'" % d[0].isoformat() for d in data])
    by_date_y = ", ".join([str(d[1]) for d in data])
    del days, data
    # By Weekday Hour
    weekdays = [x.weekday() for x in dates]
    hours = [x.hour for x in dates]
    weekday_hour_combos = zip(weekdays, hours)
    del dates
    data = sorted(Counter(weekday_hour_combos).items(),
                  key=lambda x: x[0][0] * 24 + x[0][1])
    names = ["Mon", "Tue", "Wed", "Thu", "Fri", "Sat", "Sun"]
    by_weekday_hour_x = ", ".join(
        ["'%s-%d'" % (names[d[0][0]], d[0][1]) for d in data])
    by_weekday_hour_y = ", ".join([str(d[1]) for d in data])
    # By Hour
    data = sorted(Counter(hours).items(), key=lambda x: x[0])
    by_hour_x = ", ".join([str(d[0]) for d in data])
    by_hour_y = ", ".join([str(d[1]) for d in data])
    del hours
    # By Weekday
    names = ["Monday", "Tuesday", "Wednesday",
             "Thursday", "Friday", "Saturday", "Sunday"]
    data = sorted(Counter(weekdays).items(), key=lambda x: x[0])
    by_weekday_x = ", ".join(["'%s'" % names[d[0]] for d in data])
    by_weekday_y = ", ".join([str(d[1]) for d in data])
    return render_template(
        'stats.html.j2',
        by_date_x=by_date_x, by_date_y=by_date_y,
        by_weekday_x=by_weekday_x, by_weekday_y=by_weekday_y,
        by_hour_x=by_hour_x, by_hour_y=by_hour_y,
        by_weekday_hour_x=by_weekday_hour_x, by_weekday_hour_y=by_weekday_hour_y,
        total_tweets=total_tweets(db),
        total_groups=total_groups(db),
        total_tags=total_tags(db),
        updated=datetime.utcnow().strftime("%Y-%m-%d %H:%M")
    )
