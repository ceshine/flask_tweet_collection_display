from math import ceil
from datetime import datetime
from collections import defaultdict

from flask import (
    Blueprint, flash, g, redirect, render_template, request, url_for, jsonify,
)
from werkzeug.exceptions import abort

from flaskr.db import get_db
from flaskr.cache import CACHE

PAGE_SIZE = 12

bp = Blueprint('tweets', __name__)


@CACHE.cached(timeout=300)
def get_tag_list(db):
    return [x[0] for x in db.execute("SELECT name FROM tags;").fetchall()]


def get_tweet_count(db, subquery, extras=[]):
    subquery = subquery.split("LIMIT")[0]
    return db.execute(f"SELECT COUNT(*) FROM ({subquery});", extras).fetchone()[0]


def get_group_info(db, subquery, extras=[]):
    groups_raw = db.execute(f"""
        SELECT glist.id, gstats.group_id, count FROM (
            SELECT source.id AS id, group_id FROM ({subquery}) AS source
            JOIN tweet_group_assoc AS assoc ON assoc.tweet_id = source.id) AS glist
        JOIN (
            SELECT group_id, COUNT(*) AS count FROM tweet_group_assoc GROUP BY group_id
        ) AS gstats ON glist.group_id = gstats.group_id

    """, extras).fetchall()
    groups = defaultdict(list)
    for row in groups_raw:
        groups[row[0]].append((row[1], row[2]))
    return groups


def get_tag_info(db, subquery, extras=[]):
    tags_raw = db.execute(f"""
        SELECT tweets.id, tags.name FROM ({subquery}) AS tweets
        JOIN tweet_tag_assoc AS assoc ON assoc.tweet_id = tweets.id
        JOIN tags ON assoc.tag_id = tags.id;
    """, extras).fetchall()
    tags = defaultdict(list)
    for row in tags_raw:
        tags[row[0]].append(row[1])
    return tags


@bp.route('/')
@bp.route('/page/<int:page>')
@CACHE.memoize(timeout=300)
def index(page=1):
    db = get_db()
    subquery = f"""
        SELECT * FROM tweets ORDER BY timestamp DESC, created DESC
        LIMIT {PAGE_SIZE} OFFSET ?
    """
    extras = ((page-1) * PAGE_SIZE, )
    tweets = [
        {
            "tid": row[0],
            "timestamp":datetime.utcfromtimestamp(row[1]),
            "block_quote":row[2],
            "author":row[3],
            "no_conversation":row[4],
        } for row in db.execute(
            'SELECT id, timestamp, block_quote, author, no_conversation'
            f' FROM ({subquery});', extras
        ).fetchall()
    ]
    tags = get_tag_info(db, subquery, extras)
    groups = get_group_info(db, subquery, extras)
    total_pages = ceil(get_tweet_count(db, subquery) / PAGE_SIZE)
    return render_template(
        'tweets.html.j2',
        tweets=tweets, tags=tags, groups=groups,
        base_url=url_for("tweets.index"),
        current_page=page,
        total_pages=total_pages,
        page_range=range(max(1, page-2), min(total_pages, page+2) + 1),
        all_tags=get_tag_list(db)
    )


@bp.route('/tag/<tag_name>/')
@bp.route('/tag/<tag_name>/page/<int:page>')
@CACHE.memoize(timeout=300)
def tag(tag_name, page=1):
    db = get_db()
    subquery = f"""
        SELECT *
        FROM tweets
        JOIN tweet_tag_assoc AS assoc ON tweets.id = assoc.tweet_id
        JOIN tags ON tags.id = assoc.tag_id
        WHERE tags.name = ?
        ORDER BY timestamp DESC, created DESC LIMIT {PAGE_SIZE} OFFSET ?
    """
    extras = (tag_name, (page-1) * PAGE_SIZE)
    tweets = [
        {
            "tid": row[0],
            "timestamp":datetime.utcfromtimestamp(row[1]),
            "block_quote":row[2],
            "author":row[3],
            "no_conversation":row[4],
        } for row in db.execute(f"""
            SELECT id, timestamp, block_quote, author, no_conversation
            FROM ({ subquery });
        """, extras).fetchall()
    ]
    tags = get_tag_info(db, subquery, extras)
    groups = get_group_info(db, subquery, extras)
    total_pages = ceil(get_tweet_count(db, subquery, extras[:-1]) / PAGE_SIZE)
    return render_template(
        'tag.html.j2',
        tweets=tweets, tags=tags, groups=groups,
        extra_title=f" - {tag_name}",
        base_url=url_for("tweets.tag", tag_name=tag_name),
        tag_name=tag_name,
        current_page=page,
        total_pages=total_pages,
        page_range=range(max(1, page-2), min(total_pages, page+2) + 1),
        all_tags=get_tag_list(db)
    )


@bp.route('/group/<int:group_id>/')
@bp.route('/group/<int:group_id>/page/<int:page>')
@CACHE.memoize(timeout=300)
def group(group_id, page=1):
    db = get_db()
    subquery = f"""
        SELECT tweets.*, assoc.group_id FROM tweets JOIN tweet_group_assoc AS assoc ON assoc.tweet_id = tweets.id
        WHERE assoc.group_id = ?
        ORDER BY timestamp ASC, created ASC LIMIT {PAGE_SIZE} OFFSET ?
    """
    extras = (group_id, (page-1) * PAGE_SIZE)
    tweets = [
        {
            "tid": row[0],
            "timestamp":datetime.utcfromtimestamp(row[1]),
            "block_quote":row[2],
            "author":row[3],
            "no_conversation":row[4],
        } for row in db.execute(f"""
            SELECT id, timestamp, block_quote, author, no_conversation
            FROM ({subquery});
        """, extras).fetchall()
    ]
    tags = get_tag_info(db, subquery, extras)
    total_pages = ceil(get_tweet_count(db, subquery, extras[:-1]) / PAGE_SIZE)
    return render_template(
        'tweets.html.j2',
        extra_title=" - Group (Oldest First)",
        base_url=url_for("tweets.group", group_id=group_id),
        tweets=tweets, tags=tags,
        current_page=page,
        total_pages=total_pages,
        page_range=range(max(1, page-2), min(total_pages, page+2) + 1),
        all_tags=get_tag_list(db)
    )


def search_user_subquery():
    return f"""
        SELECT * FROM tweets WHERE author = ?
        ORDER BY timestamp DESC, created DESC LIMIT {PAGE_SIZE} OFFSET ?
    """


@bp.route('/search/')
@bp.route('/search/<query>/')
@bp.route('/search/<query>/page/<int:page>')
@CACHE.memoize(timeout=300)
def search(query=None, page=1):
    db = get_db()
    if query is None:
        return jsonify(msg="No query passed!")
    query = query.strip()
    if query.startswith("@") and len(query.split(" ")) == 1:
        subquery = search_user_subquery()
        extras = (query[1:], (page-1) * PAGE_SIZE)
    else:
        subquery = f"""
            SELECT * FROM tweets
            JOIN (SELECT rowid AS id, rank FROM tweet_index WHERE block_quote MATCH ?) AS keys
            ON tweets.id = keys.id
            ORDER BY rank LIMIT {PAGE_SIZE} OFFSET ?
        """
        extras = (query, (page-1) * PAGE_SIZE)
    tweets = [
        {
            "tid": row[0],
            "timestamp":datetime.utcfromtimestamp(row[1]),
            "block_quote":row[2],
            "author":row[3],
            "no_conversation":row[4],
        } for row in db.execute(f"""
            SELECT id, timestamp, block_quote, author, no_conversation
            FROM ({subquery});
        """, extras).fetchall()
    ]
    tags = get_tag_info(db, subquery, extras)
    groups = get_group_info(db, subquery, extras)
    total_pages = ceil(get_tweet_count(db, subquery, extras[:-1]) / PAGE_SIZE)
    return render_template(
        'search.html.j2',
        tweets=tweets, tags=tags, groups=groups,
        base_url=url_for("tweets.search", query=query),
        all_tags=get_tag_list(db),
        current_page=page,
        query=query,
        total_pages=total_pages,
        page_range=range(max(1, page-2), min(total_pages, page+2) + 1)
    )


@bp.route('/tweet/<int:tid>/')
@CACHE.memoize(timeout=86400)
def tweet(tid):
    db = get_db()
    subquery = f"SELECT * FROM tweets WHERE id = ?"
    extras = (tid, )
    groups = get_group_info(db, subquery, extras)
    row = db.execute(f"""
            SELECT id, timestamp, block_quote, author, no_conversation
            FROM ({subquery});
        """, extras).fetchone()
    tweet = {
        "tid": row[0],
        "timestamp": datetime.utcfromtimestamp(row[1]),
        "block_quote": row[2],
        "author": row[3],
        "no_conversation": row[4],
    }
    tags = get_tag_info(db, subquery, extras)
    return render_template(
        'tweet.html.j2',
        tweet=tweet, tags=tags, groups=groups,
        all_tags=get_tag_list(db)
    )
