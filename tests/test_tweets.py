import pytest
from flaskr.db import get_db


def test_index(client):
    response = client.get('/')
    assert response.status_code == 200
    assert b'by Smerity on <a href' in response.data
