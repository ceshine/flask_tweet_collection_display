INSERT INTO tweets (
    reply_to_sname, reply_to_tid, no_conversation, author, block_quote,
    timestamp, id, created
) VALUES (
    NULL, NULL, 0, 'NandoDF', '<p dir="ltr" lang="en">Generative design —- this could be promising, and certainly fun <a href="https://t.co/mFoUT1LA1P">https://t.co/mFoUT1LA1P</a></p> — Nando de Freitas (@NandoDF)  <a href="https://twitter.com/NandoDF/status/1009921622801776640?ref_src=twsrc%5Etfw">June 21, 2018</a>',
    1529619052, 1009921622801776640, 1529619051);

INSERT INTO tweets (
    reply_to_sname, reply_to_tid, no_conversation, author, block_quote,
    timestamp, id, created
) VALUES (
    NULL, NULL, 1,
    'Smerity', '<p dir="ltr" lang="en">As deep learning progressed faster and I understood more of it, I''ve realized it changed my concept of what data is and how it will be treated. Data won''t stay static. Data will be given its own compute and agency, allowing itself to iteratively refine understanding over time.</p> — Smerity (@Smerity)  <a href="https://twitter.com/Smerity/status/1001224559595409409?ref_src=twsrc%5Etfw">May 28, 2018</a>',
     1527545511, 1001224559595409409, 1529619055);

INSERT INTO tweets (
    reply_to_sname, reply_to_tid, no_conversation, author, block_quote,
    timestamp, id, created
) VALUES (
    'Smerity', 1001228319898066944, 1,
    'Smerity', '<p dir="ltr" lang="en">The same patterns done by humans manually today will become transformations on underlying data. When it''s automated there won''t be a clear delineation between software and data. A video can become a thumbnail or soundscape, the video processed for animals or faces, and ...</p> — Smerity (@Smerity)  <a href="https://twitter.com/Smerity/status/1001232275005095936?ref_src=twsrc%5Etfw">May 28, 2018</a>',
     1527547351, 1001232275005095936, 1529619066);


INSERT INTO tags (name) VALUES ('tool');
INSERT INTO tags (name) VALUES ('research');

INSERT INTO tweet_tag_assoc (tag_id, tweet_id) VALUES (
    1, 1009921622801776640);
INSERT INTO tweet_tag_assoc (tag_id, tweet_id) VALUES (
    2, 1001224559595409409);
INSERT INTO tweet_tag_assoc (tag_id, tweet_id) VALUES (
    1, 1001232275005095936);
INSERT INTO tweet_tag_assoc (tag_id, tweet_id) VALUES (
    2, 1001232275005095936);
