from pathlib import Path
import os
import tempfile

import pytest
from flaskr import create_app
from flaskr.db import get_db

with open(Path(__file__).resolve().parent / 'data.sql', 'rb') as f:
    _data_sql = f.read().decode('utf8')


@pytest.fixture
def app():
    db_fd, db_path = tempfile.mkstemp()

    app = create_app({
        'TESTING': True,
        'DATABASE': db_path,
    })

    with app.app_context():
        db = get_db()
        with open(Path(__file__).resolve().parent / 'schema.sql', "rb") as f:
            db.executescript(f.read().decode('utf8'))
        db.executescript(_data_sql)

    yield app

    os.close(db_fd)
    os.unlink(db_path)


@pytest.fixture
def client(app):
    return app.test_client()


@pytest.fixture
def runner(app):
    return app.test_cli_runner()
