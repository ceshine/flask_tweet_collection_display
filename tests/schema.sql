DROP TABLE IF EXISTS tweets;
DROP TABLE IF EXISTS tags;
DROP TABLE IF EXISTS tweet_tag_assoc;

CREATE TABLE tweets (
    id UNSIGNED BIG INT PRIMARY KEY,
    timestamp INTEGER NOT NULL,
    created INTEGER NOT NULL,
    block_quote TEXT NOT NULL,
    author TEXT NOT NULL,
    no_conversation TINYINT DEFAULT 0,
    reply_to_tid UNSIGNED BIG INT,
    reply_to_sname TEXT
);

CREATE TABLE tags (
    id   INTEGER PRIMARY KEY AUTOINCREMENT,
    name TEXT    NOT NULL
);

CREATE TABLE tweet_tag_assoc (
    id       INTEGER PRIMARY KEY AUTOINCREMENT,
    tweet_id [UNSIGNED BIG INTEGER],
    tag_id   INTEGER,
    CONSTRAINT unique_tag UNIQUE (tag_id, tweet_id),
    FOREIGN KEY (tag_id) REFERENCES tags (id),
    FOREIGN KEY (tweet_id) REFERENCES tweets (id)
);

CREATE TABLE groups (
    id INTEGER PRIMARY KEY AUTOINCREMENT,
    info TEXT
);

CREATE TABLE tweet_group_assoc (
    id INTEGER PRIMARY KEY AUTOINCREMENT,
    tweet_id UNSIGNED BIG INTEGER,
    group_id INTEGER,
    CONSTRAINT unique_tweet UNIQUE (group_id, tweet_id),
    FOREIGN KEY(tweet_id) REFERENCES tweets(id) ON DELETE CASCADE,
    FOREIGN KEY(group_id) REFERENCES groups(id) ON DELETE CASCADE
);
